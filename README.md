Green Swarm: Greener Routes with Bio-inspired Techniques
========================================================

This article proposes a mobility architecture, called Green Swarm, to reduce greenhouse gas emissions from road traffic in smart cities. The traffic flow optimization of four European cities: Malaga, Stockholm, Berlin, and Paris, is addressed with new case studies importing each city's actual roads and traffic lights from OpenStreetMap into the SUMO traffic simulator, so as to find the best ways to redirect the traffic flow, and advise drivers. Additionally, the proposal is compared with three other strategies, which are also combined with Green Swarm in order to improve metrics such as travel times, gas emissions, and fuel consumption. This results in reductions in gas emissions as well as in travel times and fuel consumption in more than 500 city scenarios. The proposal has also been tested in scenarios where not all drivers are using it, to observe the change in traffic conditions when it is only in partial use, successfully paving the way for future sustainable cities.


Daniel H. Stolfi and Enrique Alba.
**Green Swarm: Greener Routes with Bio-inspired Techniques.**
*In: Applied Soft Computing, vol. 71, pp. 952-963, 2018.*
doi> https://doi.org/10.1016/j.asoc.2018.07.032


Maps of Malaga, Berlin, Stockholm, and Paris used in the article. Requires SUMO Version 0.25.0 (http://www.dlr.de)


More info in:
- https://en.danielstolfi.com/greenswarm/index.php
- https://en.danielstolfi.com/investigacion/problemas.php

Contents:
---------

- /alameda/           : Alameda case study (Malaga)
  - alameda.sumocfg   : Alameda sumo file
  - alameda.rou.xml   : routes
  - alameda.tt.sumocfg: Alameda sumo file (travel time)
  - alameda.tt.rou.xml: routes (travel time)
  - alameda.net.xml   : sumo net file
  - alameda.poly.xml  : polylines
  - alameda.add.xml   : trafic light programs
- /berlin/           : Berlin case study
  - berlin.sumocfg   : Berlin sumo file
  - berlin.rou.xml   : routes
  - berlin.tt.sumocfg: Berlin sumo file (travel time)
  - berlin.tt.rou.xml: routes (travel time)
  - berlin.net.xml   : sumo net file
  - berlin.poly.xml  : polylines
- /malaga/           : Malaga case study
  - malaga.sumocfg   : Malaga sumo file
  - malaga.rou.xml   : routes
  - malaga.tt.sumocfg: Malaga sumo file (travel time)
  - malaga.tt.rou.xml: routes (travel time)
  - malaga.net.xml   : sumo net file
  - malaga.poly.xml  : polylines
  - zona1.add.xml    : trafic light programs
  - zona2.add.xml    : trafic light programs
  - zona3.add.xml    : trafic light programs
- /paris/           : Paris case study
  - paris.sumocfg   : Paris sumo file
  - paris.rou.xml   : routes
  - paris.tt.sumocfg: Paris sumo file (travel time)
  - paris.tt.rou.xml: routes (travel time)
  - paris.net.xml   : sumo net file
  - paris.poly.xml  : polylines
- /stockholm/           : Stockholm case study
  - stockholm.sumocfg   : Stockholm sumo file
  - stockholm.rou.xml   : routes
  - stockholm.tt.sumocfg: Stockholm sumo file (travel time)
  - stockholm.tt.rou.xml: routes (travel time)
  - stockholm.net.xml   : sumo net file
  - stockholm.poly.xml  : polylines


Acknowledgments:
----------------

This research was partially funded by the Spanish MINECO and FEDER projects TIN2014-57341-R, TIN2016-81766-REDT, and TIN2017-88213-R.
Daniel H. Stolfi was supported by a grant (FPU13/00954) from the Spanish Ministry of Education, Culture and Sports.
